#Alaric DP_RX pinout

set_location_assignment PIN_V28 -to clock_ref_135_mhz
set_instance_assignment -name IO_STANDARD LVDS -to clock_ref_135_mhz
set_location_assignment PIN_V27 -to "clock_ref_135_mhz(n)"

set_location_assignment PIN_AP6 -to dp_rx_aux_in
set_location_assignment PIN_AP7 -to dp_rx_aux_oe
set_location_assignment PIN_AL3 -to dp_rx_aux_out
set_location_assignment PIN_AM3 -to dp_rx_hpd
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_in
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_oe
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_out
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_hpd

set_location_assignment PIN_AA30 -to "dp_rx_data[0]"
set_location_assignment PIN_AA29 -to "dp_rx_data[0](n)"
set_location_assignment PIN_Y32  -to "dp_rx_data[1]"
set_location_assignment PIN_Y31  -to "dp_rx_data[1](n)"
set_location_assignment PIN_W30  -to "dp_rx_data[2]"
set_location_assignment PIN_W29  -to "dp_rx_data[2](n)"
set_location_assignment PIN_V32  -to "dp_rx_data[3]"
set_location_assignment PIN_V31  -to "dp_rx_data[3](n)"
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[0]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[1]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[2]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[3]

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[0]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[1]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[2]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[3]  -entity pyrrha_fpga

set_location_assignment PIN_P28 -to clock_ref_100_mhz
set_location_assignment PIN_P27 -to "clock_ref_100_mhz(n)"
set_instance_assignment         -name IO_STANDARD LVDS -to clock_ref_100_mhz

set_location_assignment PIN_F5 -to reset_n
set_instance_assignment        -name IO_STANDARD "1.8 V" -to reset_n

set_location_assignment PIN_V8 -to led[0]
set_location_assignment PIN_V9 -to led[1]
set_location_assignment PIN_W9 -to led[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to led

#============================

set_location_assignment PIN_AE33  -to "dp_tx_data[0](n)"
set_location_assignment PIN_AE34  -to "dp_tx_data[0]"   
set_location_assignment PIN_AC33  -to "dp_tx_data[1](n)"
set_location_assignment PIN_AC34  -to "dp_tx_data[1]"   
set_location_assignment PIN_AA33  -to "dp_tx_data[2](n)"
set_location_assignment PIN_AA34  -to "dp_tx_data[2]"   
set_location_assignment PIN_W33   -to "dp_tx_data[3](n)"
set_location_assignment PIN_W34   -to "dp_tx_data[3]"   

set_location_assignment PIN_AM8 -to dp_tx_hpd
set_location_assignment PIN_AH10 -to dp_tx_aux_in
set_location_assignment PIN_AJ7 -to dp_tx_aux_out
set_location_assignment PIN_AJ6 -to dp_tx_aux_oe

set_instance_assignment -name IO_STANDARD "1.8 V"                       -to  dp_tx_aux_in         -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "1.8 V"                       -to  dp_tx_aux_oe         -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "1.8 V"                       -to  dp_tx_aux_out        -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "1.8 V"                       -to  dp_tx_hpd            -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[0]"       -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[0](n)"    -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[1]"       -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[1](n)"    -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[2]"       -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[2](n)"    -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[3]"       -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to "dp_tx_data[3](n)"    -entity pyrrha_fpga
