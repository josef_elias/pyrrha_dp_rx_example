#Alaric DP_RX pinout

set_location_assignment PIN_N29 -to clock_ref_135_mhz
set_instance_assignment -name IO_STANDARD LVDS -to clock_ref_135_mhz
set_location_assignment PIN_N28 -to "clock_ref_135_mhz(n)"

set_location_assignment PIN_G6 -to dp_rx_aux_in
set_location_assignment PIN_G5 -to dp_rx_aux_oe
set_location_assignment PIN_E6 -to dp_rx_aux_out
set_location_assignment PIN_D6 -to dp_rx_hpd
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_in
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_oe
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_out
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_hpd

set_location_assignment PIN_AN32 -to "dp_rx_data[0](n)"
set_location_assignment PIN_AN33 -to "dp_rx_data[0]"   
set_location_assignment PIN_AM30 -to "dp_rx_data[1](n)"
set_location_assignment PIN_AM31 -to "dp_rx_data[1]"   
set_location_assignment PIN_AM34 -to "dp_rx_data[2](n)"
set_location_assignment PIN_AM35 -to "dp_rx_data[2]"   
set_location_assignment PIN_AL32 -to "dp_rx_data[3](n)"
set_location_assignment PIN_AL33 -to "dp_rx_data[3]"   
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[0]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[1]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[2]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[3]

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[0]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[1]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[2]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[3]  -entity pyrrha_fpga

set_location_assignment PIN_AL10 -to clock_ref_100_mhz
set_location_assignment PIN_AM10 -to "clock_ref_100_mhz(n)"
set_instance_assignment -name IO_STANDARD "1.8 V" -to clock_ref_100_mhz

set_location_assignment PIN_AV21 -to reset_n
set_instance_assignment -name IO_STANDARD "1.8 V" -to reset_n

set_location_assignment PIN_AR23 -to led[0]
set_location_assignment PIN_AR22 -to led[1]
set_location_assignment PIN_AM21 -to led[2]
set_instance_assignment -name IO_STANDARD "1.8 V" -to led
