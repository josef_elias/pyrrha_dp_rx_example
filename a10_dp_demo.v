module a10_dp_demo#(
    parameter PIXELS_IN_PARALLEL = 2,
    parameter BITS_PER_COLOR_PLANE = 10,
    parameter COLOR_PLANES = 3
)(
    input clock_ref_100_mhz, clock_ref_135_mhz, reset_n,

    input  [3:0] dp_rx_data,
    output dp_rx_hpd,
    input  dp_rx_aux_in,
    output dp_rx_aux_out,
    output dp_rx_aux_oe,

    output [2:0] led
);


wire dp_rx_hpd_tmp;

wire clock_50_mhz;
wire clock_16_mhz;

reg video_out_valid;

wire   pll_reset = ~reset_n;

reg   [10:0] tempout;
reg   [31:0] temp;
reg   eoc, eoc_d1;
reg   over_50, over_100;

assign led[0] = video_out_valid;
assign led[1] = over_100;
assign led[2] = over_50;

assign dp_rx_hpd = dp_rx_hpd_tmp;

video_pll_a10 video_pll_a10_i
(
    .refclk( clock_ref_135_mhz ),
    .rst( pll_reset ),

    .outclk_0( clock_16_mhz ),
    .outclk_1( clock_50_mhz ),
    .outclk_2( clk_vid ),
    .outclk_3( clock_25_mhz ),
    .locked( video_pll_locked )
);


temp_sense_0 t0 (
    .corectl (1'b1), 
    .reset   (pll_reset), 
    .tempout (tempout),
    .eoc     (eoc)
);

always @(posedge clock_50_mhz or posedge pll_reset)
begin
    if (pll_reset == 1)begin
        eoc_d1   <= '0;
        temp     <= '0;
        over_50  <= 1'b0;
        over_100 <= 1'b0;
    end else begin
        eoc_d1 <= eoc;
        if ((eoc_d1 == 1'b1) & (eoc == 1'b0)) begin
            temp    <= ((tempout*693)>>10) - 265 ;
        end
        
        over_50 <= 1'b0;
        if (temp > 30'd50) begin
            over_50 <= 1'b1;
        end
        
        over_100 <= 1'b0;
        if (temp > 30'd100) begin
            over_100 <= 1'b1;
        end        
    end
        
end

dp_loopback u0 (
    .clock_100mhz_clk     (clock_ref_100_mhz),
    .clock_135mhz_clk     (clock_ref_135_mhz),
    .clock_300mhz_clk     (clk_vid),
    .clock_50mhz_clk      (clock_50_mhz),
    .clock_16mhz_clk      (clock_16_mhz),
    .dp_pll_locked_export (video_pll_locked),
    
    .dp_rx_data           (dp_rx_data),
    .dp_rx_hdp            (dp_rx_hpd_tmp),
    .dp_rx_in             (dp_rx_aux_in),
    .dp_rx_oe             (dp_rx_aux_oe),
    .dp_rx_out            (dp_rx_aux_out),
    .dp_rx_cable_detect   (1'b1),
    .dp_rx_pwr_detect     (1'b1),

    .rx_locked_lock       (video_out_valid),
    .reset_n_reset_n      (reset_n)
);

/*
dp_loopback_intel u0 (
    .clock_100mhz_clk     (clock_ref_100_mhz),
    .clock_135mhz_clk     (clock_ref_135_mhz),
    .clock_300mhz_clk     (clk_vid),
    .clock_50mhz_clk      (clock_50_mhz),
    .clock_16mhz_clk      (clock_16_mhz),
    .dp_pll_locked_export (video_pll_locked),
    
    .dp_rx_data           (dp_rx_data),
    .dp_rx_hpd            (dp_rx_hpd_tmp),
    .dp_rx_in             (dp_rx_aux_in),
    .dp_rx_oe             (dp_rx_aux_oe),
    .dp_rx_out            (dp_rx_aux_out),
    .dp_rx_cable_detect   (1'b1),
    .dp_rx_pwr_detect     (1'b1),

    .rx_locked_lock       (video_out_valid),
    .reset_n_reset_n      (reset_n)
);*/

endmodule
