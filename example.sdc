
set_time_format -unit ns -decimal_places 3

#**************************************************************
# Create Clocks
#**************************************************************

derive_clock_uncertainty

# Create the top-level clock pins
create_clock -name {clock_ref_100_mhz} -period "100MHz" {clock_ref_100_mhz}
create_clock -name {clock_ref_135_mhz} -period "135MHz" {clock_ref_135_mhz}

# Get the clocks from the different PLLs
derive_pll_clocks

# False Paths
set_clock_groups -asynchronous \
-group {clock_ref_100_mhz} \
-group [get_clocks {video_pll_a10_i|*|outclk0}] \
       [get_clocks {video_pll_a10_i|*|outclk1}] \
-group [get_clocks {video_pll_a10_i|*|outclk2}] \
-group [get_clocks {video_pll_a10_i|*|outclk3}]

#Altera 50MHz
set_false_path -from [get_clocks {altera_ts_clk}] -to [get_clocks {video_pll_a10_i|iopll_0|outclk1}]

#Setting LED outputs as false path, since no timing requirement
set_false_path -from * -to [get_ports led[*]]

# Constraining JTAG interface
# TCK port
create_clock -name altera_reserved_tck -period 100 [get_ports altera_reserved_tck]
# cut all paths to and from tck
set_clock_groups -exclusive -group [get_clocks altera_reserved_tck]
# constrain the TDI port
set_input_delay -clock altera_reserved_tck 20 [get_ports altera_reserved_tdi]
# constrain the TMS port
set_input_delay -clock altera_reserved_tck 20 [get_ports altera_reserved_tms]
# constrain the TDO port
set_output_delay -clock altera_reserved_tck 20 [get_ports altera_reserved_tdo]

set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFDenominator[*]*} -to {*} -setup -end 2
set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFDenominator[*]*} -to {*} -hold -end 2
set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFStage[*]*} -to {*} -setup -end 2
set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFStage[*]*} -to {*} -hold -end 2
set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFNumerator[*]*} -to {*} -setup -end 2
set_multicycle_path -from {*dp_rx_intel*bitec_fpll_cntrl_i|*|divider|divider|DFFNumerator[*]*} -to {*} -hold -end 2
