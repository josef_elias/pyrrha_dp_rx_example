/**
* @file dcfifo_wrapper.sv
*
* @copyright Copyright (c) 2020 NDS Surgical Imaging, LLC., All Rights Reserved
* THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF NDS Surgical Imaging, LLC.
* This software is copyrighted by and is the sole property of NDS Surgical
* Imaging, LLC., Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* @brief This is wrapper for intel module dcfifo_mixed_widths.
*/

module dcfifo_wrapper #(
    parameter DATA_WRITE_WIDTH = 32,
    parameter DATA_READ_WIDTH = 32,
    parameter FIFO_SIZE = 32,
    parameter RAM_BLOCK_TYPE = "Auto",
    localparam USEDW_WRITE_WIDTH = $clog2(FIFO_SIZE),
    localparam USEDW_READ_WIDTH = $clog2(FIFO_SIZE * DATA_WRITE_WIDTH / DATA_READ_WIDTH)
) (
    input logic clock_write,
    input logic clock_read,
    input logic reset,
    input logic enable_read,
    input logic enable_write,
    input logic [DATA_WRITE_WIDTH-1:0] data_write,
    output logic [USEDW_WRITE_WIDTH:0] remaining_write, // 3 cycle latency
    output logic full_write,
    output logic [DATA_READ_WIDTH-1:0] data_read,
    output logic [USEDW_READ_WIDTH:0] usedw_read, // 1 cycle latency
    output logic empty_read
);

localparam DEVICE_FAMILY = "Arria 10";

logic wrreq_reg, wrreq_reg_d1, wrfull, wrfull_reg, wrfull_d1, wrfull_dd1;
logic almost_wrfull;
logic reset_write, reset_read;
logic [DATA_WRITE_WIDTH-1:0] data_write_reg;
logic [DATA_READ_WIDTH-1:0] data_read_int, data_read_buff, data_read_reg;
logic [USEDW_WRITE_WIDTH-1:0] wrusedw;
logic [USEDW_WRITE_WIDTH:0] remaining_write_reg;
logic [USEDW_READ_WIDTH-1:0] rdusedw;
logic [USEDW_READ_WIDTH:0] rdusedw_full, rdusedw_reg;
logic rdreq, rdempty, rdfull;
logic data_read_valid, data_read_buff_valid;

reset_synchronizer reset_synchronizer_write_0 (
    .reset_in( reset ),
    .clock( clock_write ),
    .reset_out( reset_write )
);

reset_synchronizer reset_synchronizer_read_0 (
    .reset_in( reset ),
    .clock( clock_read ),
    .reset_out( reset_read )
);

// write logic
// almost full if last 1 space in memory left and another writes wait
// in input buffer or full flag was set and another write request is 
// proceeding (for case when wrused == 0 but last cycle was full)
assign almost_wrfull = (wrusedw > (FIFO_SIZE - wrreq_reg - wrreq_reg_d1 - 2)) | (wrfull_reg & wrreq_reg);

always @(posedge clock_write or posedge reset_write)
begin
    if (reset_write == 1) begin
        wrreq_reg <= 0;
        wrreq_reg_d1 <= 0;
        data_write_reg <= 0;
        wrfull_reg <= 1;
        wrfull_dd1 <= 0;
    end else begin
        // request and data register
        // register write req only if wasnt set full flag
        wrreq_reg <= enable_write & ~wrfull_reg;
        data_write_reg <= data_write;
        // write full flag
        // deassert full flag if wrfull from fifo is low and
        // in last cycle wasnt almost full set
        wrfull_reg <= wrfull | wrfull_dd1;
        wrfull_dd1 <= 0;
        if (enable_write & ~wrfull_reg) begin
            wrfull_reg <= almost_wrfull;
            wrfull_dd1 <= almost_wrfull;
        end;
        // data was written to fifo
        wrreq_reg_d1 <= wrreq_reg;
    end
end

// remaining_write calculation
always @(posedge clock_write or posedge reset_write)
begin
    if (reset_write == 1) begin
        remaining_write_reg <=  0;
        wrfull_d1 <=  0;
    end else begin
        wrfull_d1 <= wrfull;
        if (wrfull_d1) begin
            remaining_write_reg <=  0;
        end else begin
            remaining_write_reg <= FIFO_SIZE - wrusedw;
        end
    end
end

// write output port
assign remaining_write = remaining_write_reg;
assign full_write = wrfull_reg;

// dual clock fifo with different data in/out width
dcfifo_mixed_widths #(
    .intended_device_family(DEVICE_FAMILY),
    .lpm_numwords(FIFO_SIZE),
    .lpm_showahead("OFF"),
    .lpm_type("dcfifo"),
    .lpm_width(DATA_WRITE_WIDTH),
    .lpm_widthu(USEDW_WRITE_WIDTH),
    .lpm_widthu_r(USEDW_READ_WIDTH),
    .lpm_width_r(DATA_READ_WIDTH),
    .overflow_checking("ON"),
    .rdsync_delaypipe(5),
    .underflow_checking("ON"),
    .use_eab("ON"),
    .read_aclr_synch("ON"),
    .wrsync_delaypipe(5),
    .delay_wrusedw(1),
    .delay_rdusedw(0),
    .ram_block_type(RAM_BLOCK_TYPE),
    .lpm_hint("DISABLE_DCFIFO_EMBEDDED_TIMING_CONSTRAINT=TRUE")
) fifo_456987123 (
    .aclr( reset_write ),
    .wrclk( clock_write ),
    .wrreq( wrreq_reg ),
    .wrfull( wrfull ),
    .wrusedw( wrusedw ),
    .data( data_write_reg ),
    .rdclk( clock_read ),
    .rdempty( rdempty ),
    .rdfull( rdfull ),
    .rdreq( rdreq ),
    .rdusedw( rdusedw ),
    .q( data_read_int )
);

// read logic
// read request to fifo is data buffer is empty and fifo is not
assign rdreq = (~data_read_buff_valid | ~data_read_valid) & ~rdempty;

always @(posedge clock_read or posedge reset_read)
begin
    if (reset_read == 1) begin
        data_read_valid <= 0;
        data_read_buff <= 0;
        data_read_buff_valid <= 0;
        data_read_reg <= 0;
    end else begin
        // data register and buffer
        // read data from data buffer
        if (enable_read && data_read_buff_valid) begin
            data_read_reg <= data_read_buff;
            data_read_buff_valid <= 0;
            data_read_valid <= data_read_valid | rdreq;
        // read data from fifo
        end else if (enable_read && data_read_valid) begin
            data_read_reg <= data_read_int;
            data_read_valid <= rdreq;
        // store data from fifo to data buffer
        end else if (!data_read_buff_valid && data_read_valid) begin
            data_read_buff <= data_read_int;
            data_read_buff_valid <= 1;
            data_read_valid <= rdreq;
        // info about fifo data is valid and wait on output port
        end else if (rdreq) begin
            data_read_valid <= 1;
        end
    end
end

// rdusedw plus the amount of data in output buffer
assign rdusedw_full = rdfull ? (FIFO_SIZE * DATA_WRITE_WIDTH / DATA_READ_WIDTH) : rdusedw;

always @(posedge clock_read or posedge reset_read)
begin
    if (reset_read == 1) begin
        rdusedw_reg <= 0;
    end else begin
        rdusedw_reg <= rdusedw_full + data_read_valid + data_read_buff_valid;
    end
end

// read output ports
assign data_read = data_read_reg;
assign usedw_read = rdusedw_reg;
assign empty_read = ~data_read_valid & ~data_read_buff_valid;

endmodule : dcfifo_wrapper
