# (c) Aldec, Inc.
# All rights reserved.
#
# Last modified: $Date: 2010-02-23 14:37:09 +0100 (Tue, 23 Feb 2010) $
# $Revision: 135800 $

# set project working directory
# cd ./../../dcfifo

# load procedures and variables
source $aldec/examples/commonscripts/procedures.do
source sim/variables.do

# create project library and clear its contents
createWorklib dcfifo_wrapper
adel -all

set QSYS_SIMDIR sim

source $QSYS_SIMDIR/rivierapro_setup.tcl

dev_com

com

# compile project's source files
vlog -sv2k5 -dbg ../../clock/reset_synchronizer/hdl/reset_synchronizer.sv
vlog -sv2k5 -dbg ../../clock/clock_crossing_freerun/hdl/clock_crossing_freerun.sv
vlog -dbg hdl/dcfifo_wrapper.sv
vlog -sv2k5 -dbg sim/dcfifo_wrapper_tb.sv

set TOP_LEVEL_NAME dcfifo_wrapper_tb

# initialize simulation
elab

# log or add signals to Waveform Viewer
logOrWave $signalList

# advance simulation
run

# uncomment following line to terminate simulation automatically from script
#endsim
