/**
* @file dcfifo_wrapper_tb.sv
*
* @copyright Copyright (c) 2020 NDS Surgical Imaging, LLC., All Rights Reserved
* THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF NDS Surgical Imaging, LLC.
* This software is copyrighted by and is the sole property of NDS Surgical
* Imaging, LLC., Any unauthorized use, duplication, transmission,
* distribution, or disclosure of this software is expressly forbidden.
*
* @brief This is testbench for intel module dcfifo_mixed_widths wrapper.
*/

`timescale 1ps / 1ps

module dcfifo_wrapper_tb;

localparam DATA_WRITE_WIDTH = 32;
localparam DATA_READ_WIDTH = 32;
localparam FIFO_SIZE = 16;
localparam CLK_WRITE_PERIOD = 600000;
localparam CLK_READ_PERIOD = 500000;
localparam WRITE_EXT_LATENCY_CLK = 0;
localparam READ_EXT_LATENCY_CLK = 0;
localparam TRANSACTION_COUNT = 200;
localparam DATA_MASK = 32'h50000;
localparam WRITE_START_TIME = 30000000;
localparam READ_START_TIME = 40000000;
localparam MAX_SIMULATION_TIME = 10000000000000;

//Internal signals declarations:
logic CLK_WRITE;
logic CLK_READ;
logic RESET;
logic FULL_WRITE, EMPTY_READ;
logic ENABLE_WRITE, ENABLE_READ;
logic [DATA_WRITE_WIDTH-1:0] DATA_WRITE;
logic [DATA_READ_WIDTH-1:0] DATA_READ;
integer data_count_in, data_count_out, read_count, write_latency_count, read_latency_count;

// Unit Under Test port map
dcfifo_wrapper #(
    .DATA_WRITE_WIDTH(DATA_WRITE_WIDTH),
    .DATA_READ_WIDTH(DATA_READ_WIDTH),
    .FIFO_SIZE(FIFO_SIZE)
) UUT (
    .clock_write( CLK_WRITE ),
    .clock_read( CLK_READ ),
    .reset( RESET ),
    .enable_read( ENABLE_READ ),
    .enable_write( ENABLE_WRITE),
    .data_write( DATA_WRITE ),
    .remaining_write( ),
    .full_write( FULL_WRITE ),
    .data_read( DATA_READ ),
    .usedw_read( ),
    .empty_read( EMPTY_READ )
);

always
begin : CLOCK_CLK_WRITE
    CLK_WRITE = 1'b0;
    #(CLK_WRITE_PERIOD/2);
    CLK_WRITE = 1'b1;
    #(CLK_WRITE_PERIOD/2);
end

always
begin : CLOCK_CLK_READ
    CLK_READ = 1'b0;
    #(CLK_READ_PERIOD/2);
    CLK_READ = 1'b1;
    #(CLK_READ_PERIOD/2);
end

// reset
always
begin : INIT_RESET // begin of stimulus process
    #0
    RESET = 1'b1;
    #8000000; //0
    RESET = 1'b0;
    #(MAX_SIMULATION_TIME - 8000000) //8000000
    $display("***********test timeout***********");
    $finish;
end

// write agent
always
begin : STIMUL_WRITE // begin of stimulus process
    #0
    RESET = 1'b1;
    ENABLE_WRITE = 1'b0;
    data_count_in = 0;
    #8000000; //0
    RESET = 1'b0;
    #(WRITE_START_TIME - 8000000); //8000000
    while (data_count_in < TRANSACTION_COUNT) begin
        @ (posedge CLK_WRITE);
        #10
        if (!FULL_WRITE) begin
            // fifo is bigger because of otput buffering
            if (((data_count_in - data_count_out) == (FIFO_SIZE + 2)) && ENABLE_WRITE) begin
                $display("***********test failed for full flag***********");
                $finish;
            end
            ENABLE_WRITE = 1'b1;
            DATA_WRITE = data_count_in | DATA_MASK;
            data_count_in = data_count_in +1;
            write_latency_count = 0;
            while (write_latency_count < WRITE_EXT_LATENCY_CLK) begin
                write_latency_count = write_latency_count +1;
                @ (posedge CLK_WRITE);
                #10;
                ENABLE_WRITE = 1'b0;
            end
        end else begin
            ENABLE_WRITE = 1'b0;
        end
    end
    @ (posedge CLK_WRITE);
    ENABLE_WRITE = 1'b0;
    #MAX_SIMULATION_TIME;
//    end of stimulus events
end // end of stimulus process

// read agent
always
begin : STIMUL_READ // begin of stimulus process
    #0
    ENABLE_READ = 1'b0;
    data_count_out = 0;
    #READ_START_TIME
    forever begin
        @ (posedge CLK_READ);
        #10
        if (ENABLE_READ) begin
            if (DATA_READ != (data_count_out | DATA_MASK)) begin
                $display("***********test failed for data read value***********");
                $finish;
            end
            data_count_out = data_count_out + 1;
            read_latency_count = 0;
            while (read_latency_count < READ_EXT_LATENCY_CLK) begin
                read_latency_count = read_latency_count +1;
                ENABLE_READ = 1'b0;
                @ (posedge CLK_READ);
                #10;
            end
        end
        if (!EMPTY_READ) begin
            if (data_count_out == (data_count_in * DATA_WRITE_WIDTH / DATA_READ_WIDTH))begin
                $display("***********test failed for empty flag on read request***********");
                $finish;
            end
            ENABLE_READ = 1'b1;
        end else begin
            ENABLE_READ = 1'b0;
            if (data_count_out == (data_count_in * DATA_WRITE_WIDTH / DATA_READ_WIDTH)) begin
                $display("***********test finish ok***********");
                @ (posedge CLK_READ);
                $finish;
            end
        end
    end
//    end of stimulus events
end // end of stimulus process

endmodule
