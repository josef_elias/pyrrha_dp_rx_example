# (c) Aldec, Inc.
# All rights reserved.
#
# Last modified: $Date: 2008-09-23 12:16:54 +0200 (Tue, 23 Sep 2008) $
# $Revision: 92945 $

set topLevel dcfifo_wrapper_tb

set runTime 350300ns

set signalList {RESET CLK_WRITE ENABLE_WRITE FULL_WRITE DATA_WRITE CLK_READ ENABLE_READ EMPTY_READ DATA_READ \
                UUT/remaining_write UUT/usedw_read UUT/almost_wrfull UUT/wrusedw UUT/rdusedw UUT/wrfull UUT/rdempty}

return
