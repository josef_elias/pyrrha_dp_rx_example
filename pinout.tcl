#135mhz_ref --OK
set_location_assignment PIN_L22  -to  clock_ref_135_mhz
set_location_assignment PIN_L21  -to "clock_ref_135_mhz(n)"
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to  clock_ref_135_mhz     -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD LVDS          -to  clock_ref_135_mhz     -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD LVDS          -to "clock_ref_135_mhz(n)" -entity pyrrha_fpga

#100mhz clock  -OK
set_location_assignment PIN_C17  -to clock_ref_100_mhz
set_location_assignment PIN_C16  -to "clock_ref_100_mhz(n)"
set_instance_assignment -name INPUT_TERMINATION DIFFERENTIAL -to clock_ref_100_mhz
set_instance_assignment -name IO_STANDARD LVDS               -to clock_ref_100_mhz
set_instance_assignment -name IO_STANDARD LVDS               -to "clock_ref_100_mhz(n)"

#dp_rx_aux -OK
set_location_assignment PIN_H5   -to  dp_rx_aux_in
set_location_assignment PIN_H6   -to  dp_rx_aux_out
set_location_assignment PIN_W2   -to  dp_rx_aux_oe
set_location_assignment PIN_T4   -to  dp_rx_hpd
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_in
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_oe
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_aux_out
set_instance_assignment -name IO_STANDARD "1.8 V" -to dp_rx_hpd

#dp_rx_data --ok
set_location_assignment PIN_H24  -to  dp_rx_data[0]
set_location_assignment PIN_H23  -to "dp_rx_data[0](n)"
set_location_assignment PIN_F24  -to  dp_rx_data[1]
set_location_assignment PIN_F23  -to "dp_rx_data[1](n)"
set_location_assignment PIN_D24  -to  dp_rx_data[2]
set_location_assignment PIN_D23  -to "dp_rx_data[2](n)"
set_location_assignment PIN_B24  -to  dp_rx_data[3]
set_location_assignment PIN_B23  -to "dp_rx_data[3](n)"

set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[0]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[1]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[2]
set_instance_assignment -name IO_STANDARD "HIGH SPEED DIFFERENTIAL I/O" -to dp_rx_data[3]

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[0]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[0]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[1]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[1]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[2]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[2]  -entity pyrrha_fpga

set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V                           -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1                             -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE              -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0  -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_0                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN                -to dp_rx_data[3]  -entity pyrrha_fpga
set_instance_assignment -name XCVR_A10_RX_LINK SR                                   -to dp_rx_data[3]  -entity pyrrha_fpga

#reset -ok
set_location_assignment PIN_T1   -to reset_n
set_instance_assignment -name IO_STANDARD "1.8 V" -to reset_n

#led -ok
set_instance_assignment -name IO_STANDARD "1.8 V" -to led
set_location_assignment PIN_C11  -to led[0]
set_location_assignment PIN_C12  -to led[1]
set_location_assignment PIN_C13  -to led[2]
set_location_assignment PIN_B13  -to led[3]
set_location_assignment PIN_C20  -to led[4]
set_location_assignment PIN_C21  -to led[5]
set_location_assignment PIN_D17  -to led[6]
set_location_assignment PIN_D18  -to led[7]


# SPI Bus - Slave
set_location_assignment PIN_C5   -to spi_miso
set_location_assignment PIN_C6   -to spi_mosi
set_location_assignment PIN_B3   -to spi_sclk
set_location_assignment PIN_B4   -to spi_ss_n
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to spi_miso -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to spi_mosi -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to spi_sclk -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to spi_ss_n -entity pyrrha_fpga

# GPIO debug interface
set_location_assignment PIN_A7   -to gpio_0_3v0
set_location_assignment PIN_B8   -to gpio_1_3v0
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to gpio_0_3v0 -entity pyrrha_fpga
set_instance_assignment -name IO_STANDARD "3.0-V LVTTL" -to gpio_1_3v0 -entity pyrrha_fpga
